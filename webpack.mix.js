const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js(['resources/js/app.ts'], 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('node_modules/open-sans-all/scss/open-sans.scss', 'public/css')
    .sass('node_modules/font-awesome/scss/font-awesome.scss','public/css')
    .copy('node_modules/open-sans-all/fonts', 'public/fonts')
    .copy('node_modules/font-awesome/fonts', 'public/fonts')
    .copy('resources/images', 'public/images')
    .browserSync({  proxy: 'localhost:80' })
    .webpackConfig({
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: "ts-loader",
                    exclude: /node_modules/,
                    options: { appendTsSuffixTo: [/\.vue$/] }
                }
            ]
        },
        resolve: {
            extensions: ["*", ".js", ".jsx", ".vue", ".ts", ".tsx"],
            alias: {
                'vue$': 'vue/dist/vue.runtime.esm.js'
            }
        }
    })

