import {mount} from '@vue/test-utils'
import expect from 'expect'
import HeaderImage from '../../../resources/js/components/HeaderImage.vue'

describe("Header Image", ()=>{
    let wrapper

    beforeEach(()=>{
        wrapper = mount(HeaderImage, {
            propsData: {
                "image-url": "images/1/Image_1.png"
            }
        });
    })

    it('should emit header-clicked event on clicking header', function () {
        wrapper.find('.header-img').trigger("click")
        expect(wrapper.emitted("header-clicked")).toBeTruthy()
    });
    it('should have a header image set', function () {
        expect(wrapper.find(".header-img").html()).toContain("images/1/Image_1.png")
    });
})
