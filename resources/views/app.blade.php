<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="favicon.ico">
        <link type="text/css" rel="stylesheet" href="{{asset('css/app.css')}}"/>
        <link type="text/css" rel="stylesheet" href="{{asset('css/vue-styles.css')}}"/>
        <title>VueBnB</title>
    </head>
    <body>
        <div id="app">

        </div>

        <script type="text/javascript">
            window.vuebnb_server_data = "{!! addslashes((json_encode($data))) !!}";
        </script>

        <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    </body>
</html>
